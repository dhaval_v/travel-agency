<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model {

	protected $fillable = array(
        'title',
        'description',
        'thumbnail'
    );

	public static $rules = array(
		'title'=>'required|min:2',
		'thumbnail'=>'required|image|mimes:jpeg,jpg,bmp,png,gif'
	);

	public function prices() {
		return $this->hasMany('App\Price');
	}

	public function photos() {
		return $this->hasMany('App\Photo');
	}

	public function orders() {
		return $this->hasMany('App\Order');
	}

}
