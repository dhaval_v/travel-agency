<?php
namespace App\Http\Controllers;
use App\Tour;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
class ToursController extends Controller {

    private $destination;

	public function __construct() {
		#parent::__construct();
		$this->destination = public_path("img/thumbnails/");
	}

	public function getIndex() {
		$types = array();

		return View::make('tours.index')
			->with('tours', Tour::all());
	}

	public function postCreate(Request $request) {
		$validator = Validator::make($request->all(), Tour::$rules);

		if ($validator->passes()) {
			$tour = new Tour;
			$tour->title = $request->get('title');
			if ($request->get('description')) $tour->description =  $request->get('description');

			$image =  $request->file('thumbnail');
			$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
            Image::make($image->getRealPath())->resize(800, 450)->save($this->destination.$filename);

            /*$upload = $image->move($destination, $filename);
            if( $upload == false ) {
                return Redirect::to('admin/tours/')
                    ->withInput()
                    ->withErrors($validator)
                    ->with('message', '图片上传失败');
            }*/

			$tour->thumbnail = 'img/thumbnails/'.$filename;
			$tour->save();

			return Redirect::to('admin/tours/index')
				->with('message', '创建成功');
		}

		return Redirect::to('admin/tours/index')
			->with('message', '创建失败')
			->withErrors($validator)
			->withInput();
	}

	public function postDestroy(Request $request) {
		$tour = Tour::find($request->get('id'));

		if ($tour) {
			File::delete('public/'.$tour->thumbnail);
			$tour->delete();
			return Redirect::to('admin/tours/index')
				->with('message', '路线删除成功');
		}

		return Redirect::to('admin/tours/index')
			->with('message', '路线删除失败');
	}

    public function getEdit($id) {
        $tour = Tour::find($id);
		return View::make('tours.edit')
        	->with('tour', $tour)
        	->with('prices', $tour->prices)
        	->with('photos', $tour->photos);
    }

	public function postUpdate(Request $request) {
		$tour = Tour::find($request->get('id'));

		if ($tour) {
			$tour->title = $request->get('title');
			$tour->description = $request->get('description');

            if ($request->hasFile('thumbnail')) {
                File::delete('public/'.$tour->thumbnail);
                $image = $request->file('thumbnail');
                $filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
                Image::make($image->getRealPath())->resize(800, 450)->save($this->destination.$filename);
                $tour->thumbnail = 'img/thumbnails/'.$filename;
            }

			$tour->save();
			return Redirect::to('admin/tours/index')->with('message', '路线信息已更新');
		}

		return Redirect::to('admin/tours/index')->with('message', '路线ID无效');
	}
}
