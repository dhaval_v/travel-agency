<?php namespace App\Http\Controllers;

use App\Tour;
use Illuminate\Support\Facades\View;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//laravel 5
		//$this->middleware('auth');
        // laravel 4 code
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}
    public function getIndex() {
        //View::make()
        return View::make('home.index')
            ->with('tours', Tour::orderBy('updated_at', 'desc')->get());
    }

    public function getTour($id) {
        $tour = Tour::find($id);
        return View::make('home.tour')
            ->with('tour', $tour)
            ->with('prices', $tour->prices)
            ->with('photos', $tour->photos);
    }
}
