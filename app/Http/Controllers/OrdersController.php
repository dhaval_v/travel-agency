<?php
namespace App\Http\Controllers;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class OrdersController extends Controller {

	public function __construct() {
		/*$this->beforeFilter('admin', array('only'=>array('getOrders', 'postManage')));*/
	}

	public function postCreate(Request $request) {
        Validator::extend('start_time', 'CustomValidator@validateStartTime');
        $validator = Validator::make($request->all(), Order::$rules);

		if ($validator->passes()) {
			$order = new Order();
            $order->tour_id = $request->get('tour_id');
			$order->date = $request->get('date');
			$order->number_of_people = $request->get('number_of_people');
			$order->name = $request->get('name');
			$order->telephone = $request->get('telephone');
			$order->email = $request->get('email');
			$order->address = $request->get('address');
			if ($request->get('others')) $order->others = $request->get('others');
			$order->save();

			return Redirect::back()
				->with('message', '订单已提交，我们会马上和您联系，谢谢！');
		}

		return Redirect::back()
			->with('message', '订单提交错误')
			->withErrors($validator)
			->withInput();
	}

	/*public function postDestroy() {
		$order = Order::find(Input::get('id'));

		if ($order) {
			$order->delete();
			return Redirect::back()
				->with('message', '订单已删除');
		}

		return Redirect::back()
			->with('message', '订单删除失败');
	}*/

    public function getOrders() {
        return View::make('orders.index')
            ->with('orders', Order::orderBy('created_at', 'DESC')->paginate(10));
    }

    public function postManage(Request $request) {
        $order = Order::find($request->get('id'));

        if ($order) {
            $order->is_confirmed = $request->get('is_confirmed');
            $order->save();
            return Redirect::back()
                ->with('message', '订单已更新');
        }

        return Redirect::back()
            ->with('message', '订单更新失败');
    }


}
