<?php
namespace App\Http\Controllers;

use App\Price;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class PricesController extends Controller {

	public function __construct() {
		#parent::__construct();
		/*$this->beforeFilter('admin');*/
	}

	public function getIndex() {
		return View::make('tours.index')
			->with('tours', Tour::all());
	}

	public function postCreate(Request $request) {
		$validator = Validator::make($request->all(), Price::$rules);

		if ($validator->passes()) {
			$price = new Price;
            $price->tour_id = $request->get('tour_id');
			$price->number_of_people = $request->get('number_of_people');
			$price->price = $request->get('price');
			$price->save();

			return Redirect::back()
				->with('message', '价格创建成功');
		}

		return Redirect::back()
			->with('message', '价格创建失败')
			->withErrors($validator)
			->withInput();
	}

	public function postDestroy(Request $request) {
		$price = Price::find($request->get('id'));

		if ($price) {
			$price->delete();
			return Redirect::back()
				->with('message', '价格删除成功');
		}

		return Redirect::back()
			->with('message', '价格删除失败');
	}

}
