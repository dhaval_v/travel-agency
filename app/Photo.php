<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model {

	protected $fillable = array(
        'tour_id',
        'name',
        'path'
    );

	public static $rules = array(
        'tour_id'=>'required|integer|min:1',
        'name'=>'required',
        'path'=>'required|image|mimes:jpeg,jpg,bmp,png,gif'
	);

	public function route() {
		return $this->belongsTo('App\Tour');
	}

}
