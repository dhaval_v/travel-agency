## Travel-Ageny Project Build up in laravel 4.1 version
Laravel v4.1 : [Travel-Agency](https://github.com/jguan/laravel-4.1-travel-agency/)
upgrade into Laravel 5.2.*

### Description:
* It's simple travel-agency booking site. It's developed in laravel 4.1 version. I'm upgraded into latest laravel version (5.2)
* Documentation about how to upgrade laravel 4.1 to latest and I've done simple documentation and reference link to here: http://dhavalv.github.io/

## How to setup project:
* first of all, clone project:
```
git clone https://dhaval_v@bitbucket.org/dhaval_v/travel-agency.git
```
* after cloning, change in travel-agency directory:
```
cd travel-agency
```
* update composer
```
composer update
```
* change .env-dist to .env and setup database and email configuration
```
mv .env-dist .env
```
* Setup database configuration into .env file and Seed the database with records
```
//migrate tables into db
php artisan migrate
//Seed the database with records
php artisan db:seed
```
Thanks